

This code allows to run simulations of Conductance Based Adaptive Exponential Integrate and Fire Neural Model (CADEX).
CADEX model was created by Tomasz Górski and Damien Depannemaecker and introduced in https://www.biorxiv.org/content/10.1101/842823v1.

To run code please install Python 3 and Brian2 Neural Simulator.
